import React from 'react';

import './App.css';
import Navbar from './components/Navbar';
import Loader from './components/layout/Loader';
import Error from './components/layout/Error';
import Products from './components/Products';
import Footer from './components/Footer';
import NoProductsFound from './components/layout/NoProductsFound';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      errorMessage: "",
    };

    this.URL = 'https://fakestoreapi.com/products';
  }

  fetchData = (url) => {
    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      fetch(url)
        .then((data) => {
          return data.json();
        })
        .then((data) => {
          this.setState({
            status: this.API_STATES.LOADED,
            products: data,
          });
        })
        .catch((error) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "An API error occurred. Please try again in a few minutes."
          });
        });
    });
  }

  componentDidMount = () => {
    this.fetchData(this.URL);
  }

  render() {
    let products = this.state.products;
    let noProducts = false;

    if ((this.state.status === this.API_STATES.ERROR) || (this.state.status === this.API_STATES.LOADED && products.length === 0)) {
      noProducts = true;
    }

    return (
      <>
        <Navbar />

        {this.state.status === this.API_STATES.LOADING && <Loader />}
        {this.state.status === this.API_STATES.ERROR && <Error errorMessage={this.state.errorMessage} />}
        {this.state.status === this.API_STATES.LOADED && products.length === 0 && <NoProductsFound />}
        {this.state.status === this.API_STATES.LOADED && products.length > 0 && <Products products={products} />}

        <Footer noProducts={noProducts} />
      </>
    );
  }
}

export default App;