import React from 'react';

import './Error.css';
import errorImg from '../../../assets/images/error.svg';

class Error extends React.Component {
    render() {
        return (
            <div className="error-container">
                <div className="error-text-container">
                    <span className="error-heading">Server Error</span>
                    <div>
                        <div className="error-message">{this.props.errorMessage}</div>
                    </div>
                </div>
                <img
                    className='error-img'
                    src={errorImg}
                    alt=''
                />
            </div>
        );
    }
}

export default Error;