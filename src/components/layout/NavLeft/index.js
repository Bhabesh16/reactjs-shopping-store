import React from 'react';

import './NavLeft.css';

class NavLeft extends React.Component {
    render() {
        return (
            <div className='navLeft'>
                <a href="/">
                    <img
                        src="https://www.svgrepo.com/show/217771/shopping-logo.svg"
                        className="logo"
                        alt=""
                    />
                </a>
            </div>
        );
    }
}

export default NavLeft;