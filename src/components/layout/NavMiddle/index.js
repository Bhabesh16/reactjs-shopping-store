import React from 'react'

import './NavMiddle.css';

class NavMiddle extends React.Component {

    render() {
        return (
            <div className="navMiddle">
                <ul id="navbar" className={!this.props.mobileNav ? 'hide' : ''}>
                    <li><a href="/" className="active nav-ele">Home</a></li>
                    <li><a href="/" className="nav-ele">Shop</a></li>
                    <li><a href="/" className="nav-ele">About</a></li>
                    <li><a href="/" className="nav-ele">Contact</a></li>
                    <li><a href="/" className="nav-ele"><i className="far fa-shopping-bag"></i></a></li>
                    <ul className="navRightContainer" id="mobileRight">
                        <li><a href="/" id="signup">Sign Up</a></li>
                        <li><a href="/" id="login">Login</a></li>
                    </ul>
                </ul>
            </div>
        );
    }
}

export default NavMiddle;