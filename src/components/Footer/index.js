import React from 'react';

import './Footer.css';

export class Footer extends React.Component {
    render() {
        return (
            <div className={`footer ${this.props.noProducts? 'newPosition' : ''}`}>
                <a href="/">
                    <img
                        src='https://www.svgrepo.com/show/217771/shopping-logo.svg'
                        className='logo'
                        alt=''
                    />
                </a>
                <div className='copyright'>
                    © Shopping Store. All Rights Reserved
                </div>
            </div>
        );
    }
}

export default Footer;